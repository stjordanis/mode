# mode

Set the mode of your devices:|- send ESC/P control codes for 80/132 cols and 6/8 lpi to printer|- redirect printer to NUL or serial port|- set serial port parameters|- do codepage operations and display status of DISPLAY|- select 40/80/132x25/28/43/50/60 screen mode or select x8/14/16 font, availability depending on your|hardware. Shift CGA display sideways.|- control and check switchar and keyboard (num/...) lock states|- control keyboard repeat rate and delay


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## MODE.LSM

<table>
<tr><td>title</td><td>mode</td></tr>
<tr><td>version</td><td>2015-11-25a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-02-27</td></tr>
<tr><td>description</td><td>Set the mode of your devices:</td></tr>
<tr><td>summary</td><td>Set the mode of your devices:|- send ESC/P control codes for 80/132 cols and 6/8 lpi to printer|- redirect printer to NUL or serial port|- set serial port parameters|- do codepage operations and display status of DISPLAY|- select 40/80/132x25/28/43/50/60 screen mode or select x8/14/16 font, availability depending on your|hardware. Shift CGA display sideways.|- control and check switchar and keyboard (num/...) lock states|- control keyboard repeat rate and delay</td></tr>
<tr><td>keywords</td><td>freedos, mode, display</td></tr>
<tr><td>author</td><td>K. Heidenstrom &lt;kheidens@actrix.gen.nz&gt;, Eric Auer &lt;eric@coli.uni-sb.de&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer eric(#)coli.uni-sb.de</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/mode/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Mode</td></tr>
</table>
